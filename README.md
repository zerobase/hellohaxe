HelloHaxe
=========

Requirements
------------

- Haxe (>2.10)
- [MassiveUnit] (MUnit)
- [Mockatoo]

To Istall MUnit
---------------

	haxelib install munit

To Test The Application
-----------------------

	haxelib run munit test -neko

To Build JavaScript Code
------------------------

	haxe -cp src -main Main -js build/main.js

then open `build/main.html` with your browser.

To Build Neko Binary
--------------------

	haxe -cp src -main Main -neko build/main.n

then run

	neko build/main.n

[MassiveUnit]: http://lib.haxe.org/p/munit
[Mockatoo]: https://github.com/misprintt/mockatoo
