package app;
import haxe.Template;

class Greeter
{
	
	private var callee : Person;
	
	public function new() {
	}
	
	public function know(person : Person) : Void 
	{
		callee = person;
	}
	
	public function greet() : String {
		var greetingTemplate = new Template("Hello ::callee::!");
		var greeting = greetingTemplate.execute( { 
			callee : callee.getFirstName()
		} );
		return greeting;
	}

}