package app;

/**
 * ...
 * @author Ishibashi Hideto
 */
class Person
{

	private var firstName : String;
	
	public function new(aFirstName : String) 
	{
		setFirstName(aFirstName);
	}
	
	public function setFirstName(aFirstName : String) : Void 
	{
		firstName = aFirstName;
	}
	
	public function getFirstName() : String
	{
		return firstName;
	}
	
}