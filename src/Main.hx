package ;
import app.Greeter;
import app.Person;

class Main 
{
	
	static function main() 
	{
		var greeter = new Greeter();
		var bob = new Person("Bob");
		greeter.know(bob);
		trace(greeter.greet());
	}
	
}