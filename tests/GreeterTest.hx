package;
import app.Greeter;
import app.Person;
import massive.munit.Assert;
import mockatoo.Mockatoo;
using mockatoo.Mockatoo;

class GreeterTest
{
	private var greeter : Greeter;
	private var bob : Person;
	
	@Before
	public function setup() : Void
	{
		greeter = new Greeter();
		bob = new Person("Bob");
	}

	@Test
	public function testGreeterKnowsGivenName() : Void 
	{
		greeter.know(bob);
		Assert.areEqual(greeter.greet(), "Hello Bob!");
	}

	@Test
	public function testGreeterQueriesPersonsName() : Void 
	{
		var person = Person.mock();
		person.getFirstName().returns("Bob");
		greeter.know(person);
		Assert.areEqual(greeter.greet(), "Hello Bob!");
		person.getFirstName().verify();
	}
}